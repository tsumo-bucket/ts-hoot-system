<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageContentRequest;

use Acme\Facades\Activity;
use Acme\Facades\General;
use App\Acme\Facades\Seo as SeoFacades;

use App\PageContent;
use App\PageContentItem;
use App\Seo;
use App\Page;
use App\PageControl;

class PageContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($page_id = $request->page_id) {
            $data = PageContent::where('page_id', 'LIKE', '%' . $page_id . '%')->orderBy('order', 'ASC')->paginate(25);
        } else {
            $data = PageContent::orderBy('order', 'ASC')->paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/dev/page_contents/index')
            ->with('title', 'PageContents')
            ->with('menu', 'page_contents')
            ->with('keyword', $request->page_id)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create($pageID = 0)
    {
        $page = Page::find($pageID);
        
        if(!$page) {
            return redirect('/');
        }
        return view('admin/dev/page_contents/create')
            ->with('title', 'Create page_content')
            ->with('page', $page)
            ->with('menu', 'page_contents');
    }
    
    public function store(PageContentRequest $request)
    {
        $input = $request->all();
        // dd($input);

        $page = Page::findOrFail($input['page_id']);
        if ($page->allow_add_contents == 1) {
			$duplicateName = false;
			$duplicateSlug = false;
		} else {
			$duplicateName = PageContent::where("name", $input["content_name"])->where("page_id", $input['page_id'])->first();
			$duplicateSlug = PageContent::where("slug", $input["slug"])->where("page_id", $input['page_id'])->first();
		}

        if ($page->allow_add_contents == 1) {
			$duplicateName = false;
			$duplicateSlug = false;
		} else {
			$duplicateName = PageContent::where("name", $input["content_name"])->where("page_id", $input['page_id'])->first();
			$duplicateSlug = PageContent::where("slug", $input["slug"])->where("page_id", $input['page_id'])->first();
		}

        if($duplicateName) {
            $response = [
                'notifTitle'=>'Content name is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } elseif($duplicateSlug) {
            $response = [
                'notifTitle'=>'Content slug is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } else {
			if ($page->allow_add_contents == 1) {
				$update['enabled'] = 1;
				$update['editable'] = 1;
				$update['allow_add_items'] = 1;
			} else {
				if(isset($input['enabled'])) $input['enabled'] = 1;
				else $input['enabled'] = 0;

				if(isset($input['editable'])) $input['editable'] = 1;
				else $input['editable'] = 0;
				
				if(isset($input['allow_add_items'])) $input['allow_add_items'] = 1;
				else $input['allow_add_items'] = 0;
			}
            //--- set order of the content being created
            $count = PageContent::where("page_id", $input['page_id'])->count();
            $input["order"] = $count + 1;

			$input['name'] = $input['content_name'];
			$content = PageContent::create($input);
			
			if ($content->allow_add_items == 1) {
				$item = new PageContentItem;
				$item->slug = 'template-default';
				$item->enabled = 1;
				$item->editable = 1;
				$item->order = 0;
				$item->content_id = $content->id;
				$item->save();
			}

            $response = [
                'notifTitle'=>'Save successful.',
                'notifMessage'=>'Redirecting to edit.',
                'notifStatus'=>'success',
                'redirect'=>route('adminPageContentsEdit', [$content->id])
            ];
        }

        // $page_content = PageContent::create($input);
        
		// $input['slug'] = General::slug($page_content->page_id,$page_content->id);
		// $page_content->update($input);

        // $log = 'creates a new page_content "' . $page_content->name . '"';
        // Activity::create($log);

        // $response = [
        //     'notifStatus'=>'success',
        //     'notifTitle'=>'Save successful.',
        //     'notifMessage'=>'Redirecting to edit.',
        //     'resetForm'=>true,
        //     'redirect'=>route('adminPageContentsEdit', [$page_content->id])
        // ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/page_contents/show')
            ->with('title', 'Show page_content')
            ->with('data', PageContent::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/page_contents/view')
            ->with('title', 'View page_content')
            ->with('menu', 'page_contents')
            ->with('data', PageContent::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = PageContent::with("pageGroup", "bgImage", "items", "controls")->findOrFail($id);
		if ($data->allow_add_items == 1) {
			$itemTemplate = PageContentItem::where('content_id', $id)->where('slug', 'template-default')->first();

			if ($itemTemplate) {
				$item = $itemTemplate;
			} else {
				$item = new PageContentItem;
				$item->slug = 'template-default';
				$item->enabled = 1;
				$item->editable = 1;
				$item->order = 0;
				$item->content_id = $data->id;
				$item->save();
			}
        }
        // return $data->pageGroup;
        return view('admin/dev/page_contents/edit')
            ->with('title', 'Edit Page Content')
            ->with('menu', 'dev-pages')
            ->with('page', $data->pageGroup)
            ->with('item', @$item)
            ->with('data', $data);
        // return view('admin/dev/page_contents/edit')
        //     ->with('title', 'Edit page_content')
        //     ->with('menu', 'page_contents')
        //     ->with('data', $data)
        //     ->with('seo', $seo);
    }
        
    //API function for ordering items
    public function order(Request $request)
    {
		// return $request->all();
        $input=[];
		$data = $request->input('page_contents');
		
        $newOrder=1;
        foreach($data as $d)
        {
            $input['order'] = $newOrder;
            $page_content = PageContent::findOrFail($d);
            $page_content->update($input);
            $newOrder++;
        }

         $response = [
            'notifTitle'=>'Order updated.',
        ];
        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
		$page = Page::findOrFail($input['page_id']);

		if ($page->allow_add_contents == 1) {
			$duplicateName = false;
			$duplicateSlug = false;
		} else {
			$duplicateName = PageContent::where("name", $input["content_name"])->where("page_id", $input['page_id'])->where('id', '!=', $id)->first();
			$duplicateSlug = PageContent::where("slug", $input["slug"])->where("page_id", $input['page_id'])->where('id', '!=', $id)->first();
		}

        if($duplicateName) {
            $response = [
                'notifTitle'=>'Page name is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } elseif($duplicateSlug) {
            $response = [
                'notifTitle'=>'Page slug is alredy in use.',
                'notifMessage'=>'',
                'notifStatus'=>'error'
            ];
        } else {

            $content = PageContent::with('controls')->find($id);
            
			$hasError = false;
            if ($content) {
				if (isset($input['control']) && count(@$input['control'])) {
					if ($this->array_has_dupes($input['name'])) {
						$hasError = true;
						$response = [
							'notifTitle'=>'Saving Failed.',
							'notifMessage'=>'Duplicate form control name.',
							'notifStatus'=>'error'
						];
					} else {
						$controlsAsOf = $content->controls->toArray();
						foreach ($input['control'] as $key => $control_id) {
							$control = $content->controls()->find($control_id);
							if (!$control) {
								$control = new PageControl;
							}
							$control->name = preg_replace('/\s+/', '', $input['name'][$key]);
							$control->label = $input['label'][$key];
							$control->type = $input['type'][$key];
							$control->order = $key;
							$control->required = 0;
							if (isset($input['required']) && count(@$input['required'])) {
								$requiredKey = array_search($control_id, $input['required']);
								if ($requiredKey != false) $control->required = 1;
							}
							if ($input['type'][$key] == 'select') $control->options_json = $input['options_json'][$key];
							$content->controls()->save($control);

							foreach ($controlsAsOf as $cKey => $item) {
								if ($item['id'] == $input['control'][$key]) $controlsAsOf[$cKey]['exists'] = true;
							}
						}
						
						foreach ($controlsAsOf as $key => $value) {
							if (!(@$value['exists'] == true)) {
								$control = $content->controls()->where('name', $value['name'])->first();
								if($control){
									$control->delete();
								}	
							}
						}
					}
				} else {
					if($content){
						if(count($content->controls)>0){
							$content->controls()->delete();
						}
					}
				}

				if (!$hasError) {
					$optionChanged = false;

					$update = [];
					$update['name'] = @$input['content_name'];
					$update['slug'] = @$input['slug'];
					
					if ($page->allow_add_contents == 1) {
						$update['enabled'] = 1;
						$update['editable'] = 1;
						$update['allow_add_items'] = 1;
						$update['slug'] = 'template-default';
					} else {
						if(isset($input['enabled'])) $update['enabled'] = 1;
						else $update['enabled'] = 0;

						if(isset($input['editable'])) $update['editable'] = 1;
						else $update['editable'] = 0;
						
						if(isset($input['allow_add_items'])) $update['allow_add_items'] = 1;
						else $update['allow_add_items'] = 0;
					}

					if ($update['allow_add_items'] != $content->allow_add_items) $optionChanged = true;
					
					$content->update($update);


					if ($optionChanged) {
						if ($update['allow_add_items'] == 1) {
							$item = PageContentItem::where('slug','template-default')->first();
							if(!$item) $item = new PageContentItem;
							$item->slug = 'template-default';
							$item->enabled = 1;
							$item->editable = 1;
							$item->order = 0;
							$item->content_id = $content->id;
							foreach ($content->items as $key => $itemAsOf) {
								if (count($itemAsOf->controls) > 0) $itemAsOf->controls()->delete();
								$itemAsOf->delete();
							}
							$item->save();
						} else {
							$item = PageContentItem::where('slug', 'template-default')->where('content_id', $content->id)->first();
							if($item){
								$item->slug = '';
								$item->save();
							}
						}
					}
					
				}
            }

			if (!$hasError) {
				$response = [
					'notifTitle'=>'Save successful.',
					'notifMessage'=>'Refreshing page.',
					'notifStatus'=>'success',
					'redirect'=>route('adminPageContentsEdit', [$content->id])
				];
			}
        }

        return response()->json($response);
	}
    function array_has_dupes($array) {
		return count($array) != count(array_unique($array));
	}
    public function seo(Request $request)
    {
        $input = $request->all();

        $data = PageContent::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();
        // dd($input);
        // dd($input);
        // $data = PageContent::whereIn('id', $input['ids'])->get();
        // $names = [];
        // foreach ($data as $d) {
        //     $names[] = $d->page_id;
        // }
        // $log = 'deletes a new page_content "' . implode(', ', $names) . '"';
        // Activity::create($log);

        // PageContent::destroy($input['ids']);
        PageContent::destroy($input['id']);
        // $response = [
        //     'notifTitle'=>'Delete successful.',
        //     'notifMessage'=>'Refreshing page.',
        //     'redirect'=>route('adminPageContents')
        // ];
        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'notifStatus'=>'success',
            'redirect'=>route('adminPageContents', $input['id'])
        ];
        return response()->json($response);
    }

    
    public function clientUpdate(Request $request, $slug, $id)
	{
        $input = $request->all();
		$page = Page::where('slug', $slug)->first();

		if ($page) {
			$content = PageContent::with('controls')->findOrFail($id);
			$form = PageContent::with('controls')->findOrFail($id);
			
			if ($page->allow_add_contents == 1) {
				$form = PageContent::where('page_id', $page['id'])->where('slug', 'template-default')->first();
			}

			$hasError = false;
			$message = '';
			foreach ($form->controls as $key => $control) {
				$contentControl = $content->controls()->where('name', $control->name)->first();
				
				if (!$contentControl) {
					$contentControl = $control->replicate();
					$contentControl->reference_id = $content->id;
				} 

				if (isset($input[$control->name])) {
					if ($control->required && $control->type != 'checkbox' && @$input[$control->name] == '') {
						$message .= $control->label . ' is required. ';
						$hasError = true;
					} else {
						$contentControl->value = @$input[$control->name];
					}
				} else {
					if (isset($input["products"])) {
						if (isset($input["products"][$control->id])) {
							$contentControl->products()->sync($input["products"][$control->id]);
						}
						$contentControl->value = "products";
					} else {
						if ($control->type == "products") {
							$contentControl->value = "products";
						} else {
							$contentControl->value = "";
						}
					}
				}
				
				$contentControl->save();
				// General::saveTranslations($input, $contentControl->name, 'App\PageContent', $contentControl->id);
			}

			if (isset($input['seo_title'])) {
				$seoData['seoable_id'] = $page->id;
				if(isset($input['seo_title'])) $seoData['title'] = $input['seo_title'];
				if(isset($input['seo_description'])) $seoData['description'] = $input['seo_description'];
				if(isset($input['seo_image'])) $seoData['image'] = $input['seo_image'];
				$seoData['seoable_type'] = 'App\Page';
				SeoFacades::seoSave($seoData, $page);
			}
			
			if ($hasError) {
				$response = [
						'notifTitle'=>'Save failed.',
						'notifMessage'=>$message,
						'notifStatus'=>'error'
					];
			} else {
				$response = [
					'notifTitle'=>'Save successful.',
					'notifStatus'=>'success',
				];
			}
		} else {
			$response = [
				'notifTitle'=>'Page not found.',
				'notifStatus'=>'error',
				'redirect'=>route('adminDashboard')
			];
		}

		return response()->json($response);
    }
    
    public function clientCreate($slug){
		$page = Page::where('slug', $slug)->first();
		if ($page->allow_add_contents == 1) {
			$data = PageContent::where('page_id', $page->id)->where('slug','template-default')->first();
		} else {
			$data = $page;
		}

		if(!$page) {
            return redirect('/');
        }

        return view('admin/page_contents/create')
            ->with('title', 'Create Page Content')
			->with('menu', 'page-' . $page->slug)
            ->with('page', $page)
            ->with('data', $data);


    }
    
    public function clientStore(Request $request){
		$input = $request->all();
		$template = PageContent::where('page_id', $input['page_id'])->where('slug','template-default')->with('controls')->first();
		// dd($input);
		$newContent = New PageContent;
		$newContent->slug = '';
		$newContent->enabled = 1;
		$newContent->editable = 1;
		$newContent->order = 0;
		$newContent->page_id = $input['page_id'];
		$newContent->allow_add_items = 1;
		$newContent->save();

		if($template->controls){
			foreach($template->controls as $control){
				$newControl = $control->replicate();
				$newControl->reference_id = $newContent->id;
				$newControl->value = @$input[$newControl->name];
				if ($newControl->type == "products") {
                    $newControl->value = "products";
                }
				$newControl->save();
				if ($newControl->type == "products") {
                    if (isset($input["products"])) {
                        $default_control = $template->controls()->where('name', $control->name)->first();
                        if (isset($input["products"][$default_control->id])) {
                            $newControl->products()->sync($input["products"][$default_control->id]);
                        }
                    }
                }
			}	
		}

		$routeData = [$newContent->pageGroup->slug,$newContent->id];
		
		$response = [
			'notifTitle'=>'Save successful.',
			'notifMessage'=>'Redirecting to edit',
			'notifStatus'=>'success',
			'redirect'=>route('adminClientPageContentEdit', $routeData)
		];
		

		return response()->json($response);
    }
    
    
	public function clientEdit($slug, $id){
		$page = Page::where('slug', $slug)->first();
		$content = PageContent::with("pageGroup", "bgImage", "clientItems")->findOrFail($id);
		$seo = $page->seo()->first();

		
		if ($page->allow_add_contents == 1) {
			$data = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();
			
			if($data) {
				foreach (@$data->controls as $key => $control) {
					$content_control = @$content->controls()->where('name', $control->name)->first();
					$control->value = @$content_control->value;
					$control->products = @$content_control->products;
				}
			}
			
		} else {
			$data = $content;
		}

		$title = '';

		if(@$data->editable == 1){
			$title = 'Edit Page Content';
		}else{
			$title = 'View Page Content';
		}
		
		return view('admin/page_contents/edit')
            ->with('title', $title)
            ->with('menu', 'page-' . $page->slug)
            ->with('page', @$data->pageGroup)
            ->with('content', $content)
            ->with('seoData', $seo)
            ->with('data', $data);
	}
	
    
    public function clientDestroy(Request $request, $slug){
		$input = $request->all();
		
		PageContent::destroy($input['ids']);
		
		$response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'notifStatus'=>'success',
            'redirect'=>route('adminClientCaboodlePage', $slug)
        ];

        return response()->json($response);
	}
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/page_contents', array('as'=>'adminPageContents','uses'=>'Admin\PageContentController@index'));
Route::get('admin/page_contents/create', array('as'=>'adminPageContentsCreate','uses'=>'Admin\PageContentController@create'));
Route::post('admin/page_contents/', array('as'=>'adminPageContentsStore','uses'=>'Admin\PageContentController@store'));
Route::get('admin/page_contents/{id}/show', array('as'=>'adminPageContentsShow','uses'=>'Admin\PageContentController@show'));
Route::get('admin/page_contents/{id}/view', array('as'=>'adminPageContentsView','uses'=>'Admin\PageContentController@view'));
Route::get('admin/page_contents/{id}/edit', array('as'=>'adminPageContentsEdit','uses'=>'Admin\PageContentController@edit'));
Route::patch('admin/page_contents/{id}', array('as'=>'adminPageContentsUpdate','uses'=>'Admin\PageContentController@update'));
Route::post('admin/page_contents/seo', array('as'=>'adminPageContentsSeo','uses'=>'Admin\PageContentController@seo'));
Route::delete('admin/page_contents/destroy', array('as'=>'adminPageContentsDestroy','uses'=>'Admin\PageContentController@destroy'));
Route::get('admin/page_contents/order', array('as'=>'adminPageContentsOrder','uses'=>'Admin\PageContentController@order'));
*/
}
